import networkx as nx
import torch as th

from .method import Method


class OneShot(Method):
    """One-shot method for graph generation."""

    def __init__(self, diffusion):
        self.diffusion = diffusion
        self.loss_fn = th.compile(diffusion.get_loss)

    def sample_graphs(self, target_num_nodes):
        # construct mask
        mask_1d = (
            th.arange(target_num_nodes.max(), device=target_num_nodes.device)[None, :]
            < target_num_nodes[:, None]
        )  # N, n
        mask = mask_1d[:, None, :] & mask_1d[:, :, None]  # N, n, n

        # sample
        sample_fn = th.compile(self.diffusion.sample)
        adjs = sample_fn(mask=mask)

        # convert to graphs
        graphs = []
        for i in range(adjs.shape[0]):
            n = target_num_nodes[i]
            adj = adjs[i, :n, :n]
            graphs.append(nx.from_numpy_array(adj.numpy(force=True)))

        return graphs

    def get_loss(self, batch):
        loss = self.loss_fn(x=batch.adj, mask=batch.mask)
        return loss, {"loss": loss.item()}
