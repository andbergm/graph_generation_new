from abc import ABC, abstractmethod

from torch import Tensor


class Method(ABC):
    """Interface for graph generation methods."""

    @abstractmethod
    def sample_graphs(self, target_num_nodes: Tensor):
        pass

    @abstractmethod
    def get_loss(self, batch):
        pass

    @property
    def device(self):
        return self._device

    def to(self, device):
        self._device = device
        return self
