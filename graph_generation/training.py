import random
from pathlib import Path
from time import time

import matplotlib.pyplot as plt
import networkx as nx
import torch as th
from hydra.core.hydra_config import HydraConfig
from omegaconf import OmegaConf
from torch.optim import Adam
from torch_scatter import scatter

import wandb

from .metrics import Metric
from .model import EMA


class Trainer:
    def __init__(
        self,
        sign_net,
        model,
        method,
        train_dataloader,
        train_graphs: list[nx.Graph],
        validation_graphs: list[nx.Graph],
        validation_metrics: list[Metric],
        cfg,
    ):
        self.train_iterator = iter(train_dataloader)
        self.train_graphs = train_graphs
        random.shuffle(validation_graphs)
        self.validation_graphs = validation_graphs
        self.validation_metrics = validation_metrics
        self.cfg = cfg

        self.device = "cuda" if th.cuda.is_available() and not cfg.debugging else "cpu"
        self.method = method.to(self.device)
        self.sign_net = sign_net.to(self.device) if sign_net is not None else None
        self.model = model.to(self.device)
        self.model_ema = EMA(self.model) if cfg.training.ema else None
        self.sign_net_ema = (
            EMA(self.sign_net) if cfg.training.ema and sign_net is not None else None
        )
        self.optimizer = Adam(self.model.parameters(), cfg.training.lr)

        # Resume from checkpoint if provided
        if cfg.run.resume_id is not None:
            path = Path(f"./checkpoints/{cfg.run.resume_id}/checkpoint_latest.pt")
            checkpoint = th.load(path)
            if self.sign_net is not None:
                self.sign_net.load_state_dict(checkpoint["sign_net_state_dict"])
            if self.sign_net_ema is not None:
                self.sign_net_ema.load_state_dict(checkpoint["sign_net_ema_state_dict"])
            self.model.load_state_dict(checkpoint["model_state_dict"])
            if self.model_ema is not None:
                self.model_ema.load_state_dict(checkpoint["model_ema_state_dict"])
            self.optimizer.load_state_dict(checkpoint["optimizer_state_dict"])
            self.step = checkpoint["step"]
            self.best_validation_score = checkpoint["best_validation_score"]
            print(f"Resuming training from step {self.step}")
        else:
            self.step = 0
            self.best_validation_score = -1

        # Wandb
        if cfg.wandb_logging:
            self.wandb_run = wandb.init(
                project="graph-generation",
                config=OmegaConf.to_container(cfg, resolve=True),
                name=cfg.run.name,
                resume=cfg.run.resume_id,
            )
            run_id = self.wandb_run.id
        else:
            run_id = wandb.util.generate_id()

        # checkpoint dir
        self.checkpoint_dir = Path(HydraConfig.get().runtime.output_dir)

        print(f"Run id: {run_id}")
        num_parameters = sum(p.numel() for p in model.parameters())
        print(f"Total number of model parameters: {num_parameters / 1e6} Million")

    def test(self):
        print(f"Testing model on {self.device}")
        self.model.eval()

        self.run_validation()

    def train(self):
        print(f"Training model on {self.device}")
        self.model.train()

        last_step = False
        while not last_step:
            self.step += 1
            last_step = self.step == self.cfg.training.num_steps

            step_start_time = time()
            batch = next(self.train_iterator)
            loss_terms = self.run_step(batch)
            if self.cfg.training.log_interval > 0 and (
                self.step % self.cfg.training.log_interval == 0 or last_step
            ):
                loss_terms["step_time"] = time() - step_start_time
                self.log(loss_terms)

            if self.cfg.validation.interval > 0 and (
                self.step % self.cfg.validation.interval == 0 or last_step
            ):
                if self.model_ema is not None:
                    # save current model and load ema
                    if self.sign_net is not None:
                        sign_net_state_dict = self.sign_net.state_dict()
                        self.sign_net_ema.load_ema()
                        self.sign_net.eval()
                    model_state_dict = self.model.state_dict()
                    self.model_ema.load_ema()
                    self.model.eval()

                    validation_score = self.run_validation()

                    # restore current model
                    if self.sign_net is not None:
                        self.sign_net.load_state_dict(sign_net_state_dict)
                        self.sign_net.train()

                    self.model.load_state_dict(model_state_dict)
                    self.model.train()
                else:
                    if self.sign_net is not None:
                        self.sign_net.eval()
                    self.model.eval()
                    validation_score = self.run_validation()
                    if self.sign_net is not None:
                        self.sign_net.train()
                    self.model.train()

                self.best_validation_score = max(
                    self.best_validation_score, validation_score
                )
                if self.cfg.training.save_checkpoint:
                    print(f"Saving checkpoint at step {self.step}")
                    checkpoint = {
                        "sign_net_state_dict": self.sign_net.state_dict()
                        if self.sign_net is not None
                        else None,
                        "sign_net_ema_state_dict": self.sign_net_ema.state_dict()
                        if self.sign_net_ema is not None
                        else None,
                        "model_state_dict": self.model.state_dict(),
                        "model_ema_state_dict": self.model_ema.state_dict()
                        if self.model_ema is not None
                        else None,
                        "optimizer_state_dict": self.optimizer.state_dict(),
                        "step": self.step,
                        "validation_score": validation_score,
                        "best_validation_score": self.best_validation_score,
                    }

                    th.save(
                        checkpoint,
                        self.checkpoint_dir / "checkpoint_latest.pt",
                    )
                    if validation_score >= self.best_validation_score:
                        th.save(
                            checkpoint,
                            self.checkpoint_dir / "checkpoint_best.pt",
                        )
                        if self.cfg.wandb_logging:
                            self.wandb_run.save(
                                str(self.checkpoint_dir / "checkpoint_best.pt"),
                                base_path=self.checkpoint_dir,
                                policy="now",
                            )

    def run_step(self, batch):
        batch = batch.to(self.device, non_blocking=True)
        loss, loss_terms = self.method.get_loss(batch)

        self.optimizer.zero_grad(set_to_none=True)
        loss.backward()
        self.optimizer.step()
        if self.model_ema is not None:
            self.model_ema.update(step=self.step)
        return loss_terms

    @th.no_grad()
    def run_validation(self):
        print(f"Running validation at {self.step} steps.")

        # Generate graphs
        target_num_nodes = [len(g) for g in self.validation_graphs]
        bs = (
            self.cfg.validation.batch_size
            if self.cfg.validation.batch_size is not None
            else self.cfg.training.batch_size
        )
        batches = [
            target_num_nodes[i : i + bs] for i in range(0, len(target_num_nodes), bs)
        ]
        predicted_graphs = []
        for batch in batches:
            predicted_graphs += self.method.sample_graphs(
                th.tensor(batch, device=self.device)
            )

        # Validate graphs
        metrics = {
            str(metric): metric(
                reference_graphs=self.validation_graphs,
                predicted_graphs=predicted_graphs,
                train_graphs=self.train_graphs,
            )
            for metric in self.validation_metrics
        }

        if self.cfg.validation.per_graph_size:
            for n in set(target_num_nodes):
                metrics.update(
                    {
                        f"{metric}_{n}": metric(
                            reference_graphs=[
                                g for g in self.validation_graphs if len(g) == n
                            ],
                            predicted_graphs=[
                                g for g in predicted_graphs if len(g) == n
                            ],
                            train_graphs=self.train_graphs,
                        )
                        for metric in self.validation_metrics
                    }
                )

        self.log(metrics)

        # Example Plots
        random.shuffle(self.validation_graphs)
        n = 2
        fig, axs = plt.subplots(n, 2, figsize=(50, 50))
        for i in range(n * n):
            G = predicted_graphs[i]
            ax = axs[i // n, i % n]
            nx.draw(
                G=G,
                ax=ax,
                pos=nx.spring_layout(G, seed=42),
            )
            ax.title.set_text(f"N = {len(G)}")
            ax.title.set_fontsize(40)
        fig.tight_layout()

        if self.cfg.wandb_logging:
            self.wandb_run.log({"examples": wandb.Image(fig)}, step=self.step)
        else:
            fig.show()

        validation_metric = [key for key in metrics.keys() if "Valid" in key][0]
        return metrics[validation_metric]

    def log(self, dict: dict):
        if self.cfg.wandb_logging:
            self.wandb_run.log(dict, step=self.step)

        for key, value in dict.items():
            print(f"{key}: {value}")

    @th.no_grad()
    def log_loss_dict(self, reduction_fraction, losses):
        """Log mean loss as well as per quartile loss"""
        quartiles = (4 * (1 - reduction_fraction)).round().long()
        logs = {}
        for key, value in losses.items():
            logs[key] = value.mean().item()

            quartile_loss = scatter(value, quartiles, reduce="mean")
            for i, sub_loss in enumerate(quartile_loss.numpy(force=True)):
                if sub_loss > 0:
                    logs[f"{key}_q{i}"] = sub_loss

        self.log(logs)
