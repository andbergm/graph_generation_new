#!/bin/bash -l

conda activate test
mkdir -p nohup_logs
nohup python main.py +run=$1 name=$1 +slurm=cscs >nohup_logs/$1.out &
