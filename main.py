import ast
import os
from collections import defaultdict
from pathlib import Path

import hydra
import networkx as nx
import numpy as np
import torch as th
import torch.multiprocessing as mp
from omegaconf import DictConfig
from torch.utils.data import DataLoader
from torch_geometric.data import Batch

import graph_generation as gg


def get_expansion_items(cfg: DictConfig, train_graphs):
    # Spectral Features
    spectrum_extractor = (
        gg.spectral.SpectrumExtractor(
            num_features=cfg.spectral.num_features,
            normalized=cfg.spectral.normalized_laplacian,
        )
        if cfg.spectral.num_features > 0
        else None
    )

    # Train Dataset
    red_factory = gg.reduction.ReductionFactory(
        contraction_family=cfg.reduction.contraction_family,
        cost_type=cfg.reduction.cost_type,
        preserved_eig_size=cfg.reduction.preserved_eig_size,
        min_red_frac=cfg.reduction.min_red_frac,
        max_red_frac=cfg.reduction.max_red_frac,
        red_threshold=cfg.reduction.red_threshold,
        rand_sort_lambda=cfg.reduction.rand_sort_lambda,
    )

    if cfg.reduction.num_red_seqs > 0:
        train_dataset = gg.data.FiniteRandRedDataset(
            adjs=[nx.to_scipy_sparse_array(G, dtype=np.float64) for G in train_graphs],
            red_factory=red_factory,
            spectrum_extractor=spectrum_extractor,
            num_red_seqs=cfg.reduction.num_red_seqs,
        )
    else:
        train_dataset = gg.data.InfiniteRandRedDataset(
            adjs=[nx.to_scipy_sparse_array(G, dtype=np.float64) for G in train_graphs],
            red_factory=red_factory,
            spectrum_extractor=spectrum_extractor,
        )

    # Dataloader
    is_mp = cfg.reduction.num_red_seqs < 0  # if infinite dataset
    train_dataloader = DataLoader(
        train_dataset,
        batch_size=cfg.training.batch_size,
        shuffle=False,
        pin_memory=True,
        collate_fn=Batch.from_data_list,
        num_workers=min(mp.cpu_count(), cfg.training.max_num_workers) * is_mp,
        multiprocessing_context="spawn" if is_mp else None,
    )

    # Model
    if cfg.spectral.num_features > 0:
        sign_net = gg.model.SignNet(
            num_eigenvectors=cfg.spectral.num_features,
            hidden_features=cfg.sign_net.hidden_features,
            out_features=cfg.model.hidden_features,
            num_layers=cfg.sign_net.num_layers,
        )
    else:
        sign_net = None

    model = gg.model.SparsePPGN(
        node_in_features=cfg.node_attributes * (1 + cfg.diffusion.self_conditioning),
        edge_in_features=cfg.edge_attributes * (1 + cfg.diffusion.self_conditioning),
        node_out_features=cfg.node_attributes,
        edge_out_features=cfg.edge_attributes,
        cond_features=2,
        node_emb_features=0 if sign_net is None else cfg.model.hidden_features,
        hidden_features=cfg.model.hidden_features,
        ppgn_features=cfg.model.ppgn_features,
        num_layers=cfg.model.num_layers,
        dropout=cfg.model.dropout,
    )

    # Diffusion
    if cfg.diffusion.name == "discrete":
        model = gg.diffusion.sparse.DiscreteGraphDiffusionModel(
            model=model,
            self_conditioning=cfg.diffusion.self_conditioning,
            num_node_categories=cfg.node_attributes,
            num_edge_categories=cfg.edge_attributes,
            num_steps=cfg.diffusion.num_steps,
        )
        diffusion = gg.diffusion.sparse.DiscreteGraphDiffusion(
            model=model,
            num_steps=cfg.diffusion.num_steps,
            num_node_categories=cfg.node_attributes,
            num_edge_categories=cfg.edge_attributes,
        )
    elif cfg.diffusion.name == "edm":
        model = gg.diffusion.sparse.EDMModel(
            model=model, self_conditioning=cfg.diffusion.self_conditioning
        )
        diffusion = gg.diffusion.sparse.EDM(
            model=model, num_steps=cfg.diffusion.num_steps
        )
    else:
        raise ValueError(f"Unknown diffusion name: {cfg.diffusion.name}")

    # Method
    method = gg.method.Expansion(
        diffusion=diffusion,
        sign_net=sign_net,
        spectrum_extractor=spectrum_extractor,
        augmented_radius=cfg.method.augmented_radius,
        augmented_dropout=cfg.method.augmented_dropout,
        deterministic_expansion=cfg.method.deterministic_expansion,
        min_red_frac=cfg.reduction.min_red_frac,
        max_red_frac=cfg.reduction.max_red_frac,
        red_threshold=cfg.reduction.red_threshold,
    )

    return {
        "train_dataloader": train_dataloader,
        "method": method,
        "model": model,
        "sign_net": sign_net,
    }


def get_one_shot_items(cfg: DictConfig, train_graphs):
    # Train Dataset
    train_dataset = gg.data.DenseGraphDataset(
        adjs=[nx.to_numpy_array(G, dtype=bool) for G in train_graphs]
    )

    train_dataloader = DataLoader(
        train_dataset,
        batch_size=cfg.training.batch_size,
        shuffle=False,
        pin_memory=True,
    )

    # Model
    model = gg.model.PPGN(
        in_features=cfg.edge_attributes * (1 + cfg.diffusion.self_conditioning),
        out_features=cfg.edge_attributes,
        cond_features=1,
        hidden_features=cfg.model.hidden_features,
        ppgn_features=cfg.model.ppgn_features,
        num_layers=cfg.model.num_layers,
        dropout=cfg.model.dropout,
    )

    # Diffusion
    if cfg.diffusion.name == "discrete":
        model = gg.diffusion.dense.DiscreteGraphDiffusionModel(
            model=model,
            self_conditioning=cfg.diffusion.self_conditioning,
            num_categories=cfg.edge_attributes,
            num_steps=cfg.diffusion.num_steps,
        )
        diffusion = gg.diffusion.dense.DiscreteGraphDiffusion(
            model=model,
            num_steps=cfg.diffusion.num_steps,
            num_categories=cfg.edge_attributes,
        )
    elif cfg.diffusion.name == "edm":
        raise NotImplementedError
    else:
        raise ValueError(f"Unknown diffusion name: {cfg.diffusion.name}")

    # Method
    method = gg.method.OneShot(diffusion=diffusion)

    return {"train_dataloader": train_dataloader, "method": method, "model": model}


@hydra.main(config_path="config", config_name="config", version_base="1.3")
def main(cfg: DictConfig):
    if cfg.debugging:
        os.environ["CUDA_LAUNCH_BLOCKING"] = "1"

    # Graphs
    if "spectre" in cfg.dataset.name:
        # The following snippet is used from "github.com/KarolisMart/SPECTRE".
        adjs = th.load(Path(cfg.adjs_path).absolute())[0]

        test_len = int(round(len(adjs) * 0.2))
        train_len = int(round((len(adjs) - test_len) * 0.8))
        validation_len = len(adjs) - train_len - test_len

        train, val, test = th.utils.data.random_split(
            adjs,
            [train_len, validation_len, test_len],
            generator=th.Generator().manual_seed(1234),
        )

        train_graphs = [
            nx.from_numpy_array(adj.numpy().astype(np.float64)) for adj in train
        ]
        if cfg.validation.testing:
            val = test
        validation_graphs = [
            nx.from_numpy_array(adj.numpy().astype(np.float64)) for adj in val
        ]

    elif cfg.dataset.name in ["planar", "tree"]:
        graph_generator = (
            gg.data.generate_planar_graphs
            if cfg.dataset.name == "planar"
            else gg.data.generate_tree_graphs
        )

        num_nodes = (
            [cfg.dataset.num_nodes]
            if isinstance(cfg.dataset.num_nodes, int)
            else cfg.dataset.num_nodes
        )
        train_graphs = []
        for lit in num_nodes:
            if isinstance(lit, int):
                min_num_nodes = lit
                max_num_nodes = lit
            else:
                min_num_nodes, max_num_nodes = ast.literal_eval(lit)
            train_graphs += graph_generator(
                num_graphs=cfg.dataset.train_size,
                min_num_nodes=min_num_nodes,
                max_num_nodes=max_num_nodes,
                seed=0,
            )

        val_num_nodes = (
            [cfg.dataset.val_num_nodes]
            if isinstance(cfg.dataset.val_num_nodes, int)
            else cfg.dataset.val_num_nodes
        )
        validation_graphs = []
        for lit in val_num_nodes:
            if isinstance(lit, int):
                min_num_nodes = lit
                max_num_nodes = lit
            else:
                min_num_nodes, max_num_nodes = ast.literal_eval(lit)
            validation_graphs += graph_generator(
                num_graphs=cfg.dataset.val_size,
                min_num_nodes=min_num_nodes,
                max_num_nodes=max_num_nodes,
                seed=2 if cfg.validation.testing else 1,
            )

    elif cfg.dataset.name == "sbm":
        train_graphs = gg.data.generate_sbm_graphs(
            num_graphs=cfg.dataset.train_size,
            min_num_communities=cfg.dataset.min_num_communities,
            max_num_communities=cfg.dataset.max_num_communities,
            min_community_size=cfg.dataset.min_community_size,
            max_community_size=cfg.dataset.max_community_size,
            seed=0,
        )
        validation_graphs = gg.data.generate_sbm_graphs(
            num_graphs=cfg.dataset.val_size,
            min_num_communities=cfg.dataset.min_num_communities,
            max_num_communities=cfg.dataset.max_num_communities,
            min_community_size=cfg.dataset.min_community_size,
            max_community_size=cfg.dataset.max_community_size,
            seed=2 if cfg.validation.testing else 1,
        )

    else:
        raise ValueError(f"Unknown dataset name: {cfg.dataset.name}")

    # Metrics
    validation_metrics = [
        gg.metrics.NodeNumDiff(),
        gg.metrics.NodeDegree(),
        gg.metrics.ClusteringCoefficient(),
        gg.metrics.OrbitCount(),
        gg.metrics.Spectral(),
        gg.metrics.Wavelet(),
        gg.metrics.Uniqueness(),
        gg.metrics.Novelty(),
    ]

    if "planar" in cfg.dataset.name:
        validation_metrics += [
            gg.metrics.ValidPlanar(),
            gg.metrics.UniqueNovelValidPlanar(),
        ]
    elif "tree" in cfg.dataset.name:
        validation_metrics += [
            gg.metrics.ValidTree(),
            gg.metrics.UniqueNovelValidTree(),
        ]
    elif "sbm" in cfg.dataset.name:
        validation_metrics += [
            gg.metrics.ValidSBM(),
            gg.metrics.UniqueNovelValidSBM(),
        ]

    # Method
    if cfg.method.name == "expansion":
        method_items = get_expansion_items(cfg, train_graphs)
    elif cfg.method.name == "one_shot":
        method_items = get_one_shot_items(cfg, train_graphs)
    else:
        raise ValueError(f"Unknown method name: {cfg.method.name}")
    method_items = defaultdict(lambda: None, method_items)

    # Trainer
    trainer = gg.training.Trainer(
        sign_net=method_items["sign_net"],
        model=method_items["model"],
        method=method_items["method"],
        train_dataloader=method_items["train_dataloader"],
        train_graphs=train_graphs,
        validation_graphs=validation_graphs,
        validation_metrics=validation_metrics,
        cfg=cfg,
    )

    if cfg.validation.testing:
        trainer.test()
    else:
        trainer.train()


if __name__ == "__main__":
    mp.set_start_method("spawn")
    main()
